import matplotlib
matplotlib.use('Agg')
import pandas as pd
import Bio.SeqIO
import numpy as np
import math
import random
import pickle
from keras.utils import np_utils
from keras import backend as K
from keras import models
from keras import activations
from vis.utils import utils
from itertools import izip
from sklearn.metrics import confusion_matrix
from sys import argv
import os.path

DIVISIONS=[argv[1]]
SUBSAMPLES=[argv[2]]

"""
DIVISIONS=['subsample_assignment1','subsample_assignment2','subsample_assignment3','subsample_assignment4']
SUBSAMPLES=['subsample1','subsample2','subsample3','subsample4','subsample5']
"""

PREDICTOR='Pro_and_Ter'
#set source of input data
SEQ_FILE_PRO='../../1_sequences_and_data_splitting/promoters.fa'
SEQ_FILE_TER='../../1_sequences_and_data_splitting/terminators.fa'
DATA_FILE='../../1_sequences_and_data_splitting/un_vs_expr.csv'
SUMMARY_DIRECTORY='../../8_trained_models_un_vs_expr_remove_codon/'
SUMMARY_FILE='SUMMARY_FILE_un_vs_expr_remove_codon'
data=pd.read_csv(DATA_FILE,sep='\t')
data=data.set_index('Geneid')


geneIDs=[]
categories=[]
seqs=[]
seqs_pro=[]
seqs_ter=[]

# read sequences
for x,y in izip(Bio.SeqIO.parse(SEQ_FILE_PRO,'fasta'), Bio.SeqIO.parse(SEQ_FILE_TER,'fasta') ):
     geneID=x.id
     seq_pro=x.seq.tostring()
     seq_ter=y.seq.tostring()
     seq=seq_pro+seq_ter
     if geneID in data.index:
         seqs.append(seq)
         seqs_pro.append(seq_pro)
         seqs_ter.append(seq_ter)
         geneIDs.append(geneID)
         category=data['category'][geneID]
         if category=='expressed':
              categories.append(0)
         else:
              categories.append(1)


geneIDs=np.array(geneIDs)
categories=np_utils.to_categorical(np.array(categories),2)

# one-hot encoding
dict={'A':[1,0,0,0],'C':[0,1,0,0],'G':[0,0,1,0],'T':[0,0,0,1]}

def one_hot_encoding(seq):
    one_hot_encoded=np.zeros(shape=(4,len(seq)))
    for i,nt in enumerate(seq):
        one_hot_encoded[:,i]=dict[nt]
    return one_hot_encoded

print "ONE-HOT ENCODING STARTED"
if PREDICTOR=="Pro_and_Ter":
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs],dtype=np.float32),3)
    one_hot_seqs[:,:,1000:1003,:]=0
    one_hot_seqs[:,:,1997:2000,:]=0
if PREDICTOR=="Pro":
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_pro],dtype=np.float32),3)
    one_hot_seqs[:,:,1000:1003,:]=0
if PREDICTOR=='Ter':
    one_hot_seqs=np.expand_dims(np.array([ one_hot_encoding(seq) for seq in seqs_ter],dtype=np.float32),3)
    one_hot_seqs[:,:,497:500,:]=0
print "ONE-HOT ENCODING FINISHED"

un_vs_expr_summary=pd.read_csv(SUMMARY_DIRECTORY+SUMMARY_FILE,sep='\t',header=None)
un_vs_expr_summary.columns=['name','splitting_id','testing_subsample','shuffle','tp','tn','fp','fn','accuracy','test_total','predictor']

def occlude(seq):
   occluded=[seq for i in range(seq.shape[1])]
   occluded=np.array(occluded)
   for i in range(seq.shape[1]):
       occluded[i,:,i,:]=0
   return occluded

for DIVISION in DIVISIONS:
   for SUBSAMPLE in SUBSAMPLES:
      row_indices=[ i for i in range(un_vs_expr_summary.shape[0]) if (un_vs_expr_summary['shuffle'][i]=='None' and un_vs_expr_summary['splitting_id'][i]==DIVISION and un_vs_expr_summary['testing_subsample'][i]==SUBSAMPLE) and un_vs_expr_summary['predictor'][i]==PREDICTOR ]
      for row_index in row_indices:
          name=un_vs_expr_summary['name'][row_index]
          splitting_id=un_vs_expr_summary['splitting_id'][row_index]
          testing_subsample=un_vs_expr_summary['testing_subsample'][row_index]
          predictor=un_vs_expr_summary['predictor'][row_index]

          testing_genes=np.array([gene for gene in geneIDs if data[splitting_id][gene]==testing_subsample])
          testing_indices=np.array([np.where(geneIDs==gene)[0][0] for gene in testing_genes])
          testing_data=one_hot_seqs[testing_indices]
          testing_categories=np.argmax(categories[testing_indices],axis=1)
          # load model
          model=models.load_model(SUMMARY_DIRECTORY+"MODEL_"+str(name))
          prediction=np.argmax(model.predict(testing_data),axis=1) 
          # modify model
          model.layers[-1].activation=activations.linear          
          model=utils.apply_modifications(model)
          # get true positives and true negatives
          tp_data=[]
          tn_data=[]
          for i,j in enumerate(testing_indices):
              testing_gene=geneIDs[j]
              if testing_categories[i]==1 and prediction[i]==1:
                  tp_data.append(testing_data[i])
              elif testing_categories[i]==0 and prediction[i]==0:
                  tn_data.append(testing_data[i])
          tp_data=np.array(tp_data)
          tn_data=np.array(tn_data)
          print len(tp_data)
          print len(tn_data)

          # calculate occlusion for each gene
          tp_occlusion_scores=[]
          tn_occlusion_scores=[]
          count=0
          for tp in tp_data:
              predicted_value=model.predict(np.expand_dims(tp,0))[0][1]
              tp_occluded=occlude(tp)
              occlusion_scores=model.predict(tp_occluded)[:,1]-predicted_value

              occlusion_scores=np.array([occlusion_scores for i in range(4)])
              occlusion_scores=occlusion_scores * np.squeeze(tp)
              tp_occlusion_scores.append(occlusion_scores)
              print "done"+DIVISION+SUBSAMPLE+"tp"+str(count)
              count=count+1
          count=0
          for tn in tn_data:
              predicted_value=model.predict(np.expand_dims(tn,0))[0][1]
              tn_occluded=occlude(tn)
              occlusion_scores=model.predict(tn_occluded)[:,1]-predicted_value

              occlusion_scores=np.array([occlusion_scores for i in range(4)])
              occlusion_scores=occlusion_scores * np.squeeze(tn)
              tn_occlusion_scores.append(occlusion_scores)
              print "done"+DIVISION+SUBSAMPLE+"tn"+str(count)
              count=count+1
          tp_occlusion_scores=np.average(np.array(tp_occlusion_scores),axis=0)
          tn_occlusion_scores=np.average(np.array(tn_occlusion_scores),axis=0)
          pd.DataFrame(tp_occlusion_scores).to_csv('occlusion_expr_'+DIVISION+SUBSAMPLE+"tp"+".csv")
          pd.DataFrame(tn_occlusion_scores).to_csv('occlusion_expr_'+DIVISION+SUBSAMPLE+"tn"+".csv")

