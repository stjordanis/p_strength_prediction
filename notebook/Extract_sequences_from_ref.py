
# coding: utf-8

# In[1]:

import sys, os
import pprint
from BCBio import GFF
from BCBio.GFF import GFFExaminer
import pandas as pd
from Bio import SeqIO
from Bio.Seq import Seq
import h5py
from sklearn.preprocessing import OneHotEncoder
import numpy
import pickle


# In[2]:

#examiner = GFFExaminer()
#in_handle = open(in_file)
#pprint.pprint(examiner.available_limits(in_handle))
#in_handle.close()


# In[3]:

def get_gene_pos(in_file):
    limit_info = dict(#gff_id = ["1"],
                      gff_type = ["gene"])
    #print limit_info
    gene_pos_df=[]
    in_handle = open(in_file)
    for rec in GFF.parse(in_handle, limit_info=limit_info):
        for feature in rec.features:
            gene=feature
            gene_pos_df.append(tuple([rec.id, gene.qualifiers['gene_id'][0], int(gene.location.start), int(gene.location.end), gene.location.strand]))
            #gene_len_dict[gene.qualifiers['gene_id'][0]] = int(gene.location.end) - int(gene.location.start)
        #print rec.features
    in_handle.close()
    gene_pos_df=pd.DataFrame(gene_pos_df, columns=['Chr','Gene_ID','Start','End','Strand'])
    return gene_pos_df#, gene_len_dict


# In[4]:

def get_gene_pos_phytozome(in_file):
    limit_info = dict(#gff_id = ["1"],
                      gff_type = ["gene"])
    #print limit_info
    gene_pos_df=[]
    in_handle = open(in_file)
    for rec in GFF.parse(in_handle, limit_info=limit_info):
        for feature in rec.features:
            gene=feature
            gene_pos_df.append(tuple([rec.id, gene.qualifiers['ID'][0], int(gene.location.start), int(gene.location.end), gene.location.strand]))
            #gene_len_dict[gene.qualifiers['gene_id'][0]] = int(gene.location.end) - int(gene.location.start)
        #print rec.features
    in_handle.close()
    gene_pos_df=pd.DataFrame(gene_pos_df, columns=['Chr','Gene_ID','Start','End','Strand'])
    return gene_pos_df#, gene_len_dict


# In[5]:

def get_strt_codon_pos_phytozome(in_file):
    limit_info = dict(#gff_id = ["Chr01"],
                      gff_type = ["CDS"])
    #print limit_info
    gene_pos_dict={}
    in_handle = open(in_file)
    for rec in GFF.parse(in_handle, limit_info=limit_info, target_lines=1):
        for feature in rec.features:
            gene=feature
            #determin how to parse gene names based on which genome they came from
            if gene.qualifiers['Parent'][0][:6]=="Sobic.":
                gene_name=gene.qualifiers['Parent'][0][:16]
            if gene.qualifiers['Parent'][0][:6]=="ZmMO17":
                gene_name=gene.qualifiers['Parent'][0].replace("t","g").split("_")[0]
            if gene.qualifiers['Parent'][0][:11]=="transcript:":
                gene_name=gene.qualifiers['Parent'][0].split("transcript:")[1].split("_")[0]
            #add genes to dictionary and determine which is the first CDS
            if gene_name not in gene_pos_dict:
                gene_pos_dict[gene_name]=tuple([rec.id,gene_name,int(gene.location.start),
                                                int(gene.location.end), gene.location.strand])
            else:
                if gene.location.strand==1:
                    if int(gene.location.start) < gene_pos_dict[gene_name][2]:
                        gene_pos_dict[gene_name]=tuple([rec.id, gene_name, int(gene.location.start),
                                                        int(gene.location.end), gene.location.strand])
                if gene.location.strand==-1:
                    if int(gene.location.end) > gene_pos_dict[gene_name][3]:
                        gene_pos_dict[gene_name]=tuple([rec.id, gene_name, int(gene.location.start),
                                                        int(gene.location.end), gene.location.strand])
    in_handle.close()
    gene_pos_df=pd.DataFrame(gene_pos_dict, index=['Chr','Gene_ID','Start','End','Strand']).T.sort_values(by="Chr")
    return gene_pos_df #, gene_len_dict


# In[6]:

def extract_region_by_strt_codon(genome, df, gene_dict):
    added_Ns=[]
    strt_codon_error=[]
    chromesome=['','']
    size=2000
    for row in range(0,len(df)):
        #print row
        #put current chromosome in memory
        if chromesome[0]!=df.iloc[row,0]:
            chromesome[0]=df.iloc[row,0]
            chromesome[1]=genome[df.iloc[row,0]]
        #extract region of interest for a specific gene
        Gene = df.iloc[row,:].values.tolist()
        if Gene[4]==1:
            #print("Pos strand", Gene)
            if chromesome[1].seq[Gene[2]:Gene[2]+3] == "ATG":
                if Gene[2] < size:
                    strt=0
                    end=Gene[2]
                else:
                    strt=Gene[2]-size
                    end=Gene[2]
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end]
            else:
                #print "WARNING: CODON START SITE NOT ATG; "+Gene[1]+" : "+chromesome[1].seq[Gene[2]:Gene[2]+3]+" GENE EXCLUDED."
                strt_codon_error.append(Gene[1])
        if Gene[4]==-1:
            #print("Neg strand", Gene)
            if chromesome[1].seq[Gene[3]-3:Gene[3]].reverse_complement() == "ATG":
                strt=Gene[3]
                end=Gene[3]+size
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end].reverse_complement()
            else:
                #print "WARNING: CODON START SITE NOT ATG; "+Gene[1]+" : "+chromesome[1].seq[Gene[3]-3:Gene[3]].reverse_complement()+" GENE EXCLUDED"
                strt_codon_error.append(Gene[1])
        if Gene[1] in gene_dict:
            if len(gene_dict[Gene[1]])<size:
                added_Ns.append(Gene[1])
                gene_dict[Gene[1]]="N"*(size-len(gene_dict[Gene[1]]))+gene_dict[Gene[1]]
    print "Good:",len(gene_dict.keys()), "N's added:", len(added_Ns), "Bad Start Codon:",len(strt_codon_error)
    return gene_dict, added_Ns,strt_codon_error


# In[7]:

def seq_to_oh(string_of_char):
    #setup IUPAC dictionary
    seq_dict={'A':0,'C':1,'G':2,'T':3,'N':4}
    numerical_list=[]
    for nuc in string_of_char:
        numerical_list.append(seq_dict[nuc])
    #convert to one-hot
    numerical_list=numpy.array(numerical_list)
    onehot_encoder = OneHotEncoder(n_values=5,sparse=False)
    integer_label = numerical_list.reshape(len(numerical_list), 1)
    onehot_out=onehot_encoder.fit_transform(integer_label).astype('int8')
    #onehot_out=numpy.expand_dims(onehot_encoder.fit_transform(integer_label).astype('int8'), axis=0)
    return onehot_out


# In[8]:

def extract_region_by_pos(genome, df, gene_dict,lengths):
    skipped=[]
    chromesome=['','']
    out=lengths[0]
    inG=lengths[1]
    for row in range(0,len(df)):
        #put current chromosome in memory
        if chromesome[0]!=df.iloc[row,0]:
            chromesome[0]=df.iloc[row,0]
            chromesome[1]=genome[df.iloc[row,0]]
        #extract gene of interest
        Gene = df.iloc[row,:].values.tolist()
        if Gene[4]==1:
            #print("Pos strand", Gene)
            if Gene[2] < out:
                skipped.append(Gene[1])
                continue # skip genes that are too near the start of the chromosome, most of these are on scaffolds.
            else:
                strt=Gene[2]-out
                end=Gene[2]+inG
                if end > len(chromesome[1]):
                    skipped.append(Gene[1])
                    continue # skip genes that are too near the end of the chromosome, most of these are on scaffolds.
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end]
        else:
            #print("Neg strand", Gene)
            if Gene[3] < inG:
                skipped.append(Gene[1])
                continue
            else:
                strt=Gene[3]-inG
                end=Gene[3]+out
                if end > len(chromesome[1]):
                    skipped.append(Gene[1])
                    continue # skip genes that are too near the end of the chromosome, most of these are on scaffolds.
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end].reverse_complement()   
    return gene_dict, skipped


# In[9]:

'''
#For checking extract_region_by_pos
gene_dict={}
in_file = "../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.gff3"
genome = '../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
gene_pos_df=get_gene_pos(in_file)
genome = SeqIO.index(genome, "fasta")
gene_dict, skipped=extract_region_by_pos(genome,gene_pos_df, gene_dict=gene_dict, lengths=[1000,500])
print len(skipped)
gene_pos_df[gene_pos_df["Gene_ID"].isin(skipped)]
#sanity check forward gene
GRMZM2G333069="CCACTCATCTATTTCTTTTGAATCATTGTATACTAGCTAGCTAGCTAGCCAGAGCTTGTATGGCCACTGCTCATCATGCGGATGACCATCGTGCAGGGAGAAGAAGCGAGGCCGTCGGAGAGAACTACATGAGGGGCCTCTACGGCGACGATGACTACAACGCCACCCACTACGGTCACCAGCAGCAGCAGCGGCCGCCGCCCGCGCCCATGGCGGTGGCCAAGGCGTTAGCCACAGCCACGGCAGCCTTCTCCATGTTGTTGTTGTCGGGGCTGGCGGTGACGGGCACGGTGCTGGCGCTCATCGTGGCGACGCCATTGATGGTGATCTTCAGCCCCGTGCTGGTGCCGGCGGCCATCACGGTGGCGCTGCTCACGGTAGGCATCGTTTCGTCGGGAGGGTTCGGCGTGGCTGCGGTGGCTGTGCTGGCGTGGGTGTACCGCTACCTTCAGACCACGACCTCGTCGTCGGGTCAGCAGCCGCACATCGTCAAGGACTGGGCGCAGCAGCACCGTCTGGAGCAGACACGCGCGCACTGAAATAAAAAAAAGCGTAGTACCCCTCCGTTTCTTTTTATTTGTTGCTGTTTAGTTCATATTTGTCACTGTTTCGTTAAAAATGAACTAGCGGGATAACGAGAACGGGAGGTAATATAGCTTTTCATTTAATGTAATGTAACGTAATCTTATGTTTGCAGCAGTAATAATGTGTATTATATATTTGCAGCAATAAAAGGGCGAGCTCGATCGACCCCTCTACGAATAAATACTTTAAAAAA"
print gene_dict["GRMZM2G333069"][-500:]==GRMZM2G333069[:500]
#reverse gene
GRMZM2G133819="AAAAAAAAAAAAACCTCTCCCTCCAAAACTCCTCTCTTCCCCTTCCGAATTACAGTTTCCCTCGCATCCTCCTCCTACATCTATCTCCCACCTCGCACAATCACAGATTCCTGATTCCTCAATCCTCAGTACCCCGCTGCCTGTCCTCCACGCACCTCACCGCAAACATGGCGAACGCCGCCGACGACCTGGACCTCCTGCTATCACTAGGCGAGGCCGTCCCGGAGACACCTCCCGCCTCCCCCCGCACCGCCGATGTCCCCGTGTGCGACGGCGCTTTCACACCACCAAGGACGGCGCCCCCCGGCGGCACCGACATGTCCGTTTTCCGCGACGCCGTCAAGGATTACCTCGAGGCGGCCCCCGAAACCACCTCCCCGCTTGCTAATCGCCCCAAGCGACCCAAGGCCACCGAAATCCTCGTCGACAAATACTCCGGCCTCCGCATCAAGCACCTTACGCTCTCGCCTCTTGAGATTAGCAACCGCTTCGCTGACATCCGATTTGTGCGCATCACCGCGTTAAAGTGAGGATCCTTTTCCAGTTCGCCCCGCTGCTGGGTCATTCATGGTTTTTGCTCATCGATTCATCGGGTGGCTTTTCTTCCCTTTTCTGTTTCGCGTGCAGGAACTCTGTGGGGAGCGACAGGTTCTCCGGCTGTTGGGCAACCGCGGGCGTGTTACTGGACAAGGGCGTGCAGCGGGTGAGTGCGAAGGGGAGCAGCTACAGCATCTGGAAGATGGGCGCTCTGGACGAGACCGACGTATCTCTGTTCCTGTTTGGGGACGCGCACGTTCACTACTCCGGAGCTGCTGTGGGTTCAGTGTTCGCTGTGTTCAATGGCAATGTCCGCATGGACAATGGGGTACTTCTTATCTGACATTTTGATTTTCTTAATCACTTGGGTGATTTGTGCTCAGTGATTCCGTTGCTGATTCGAATGCGTGCTGGTGACGTTGTGTAGGGCAAAGGGTTCTCTATGAGTGTTGCTTCTGTTGGACAGATGTTGAAGATGGGGGTCGCATCAGATTTTGGTCTCTGCAAAGGCAAGAGGAAAGACGGGGTGGCTTGCACCATGGCTATAAATAAGTATGTACCTC"
print gene_dict["GRMZM2G133819"][-500:]==GRMZM2G133819[:500]
'''


# In[10]:

def extract_region_by_pos_term(genome, df, gene_dict,lengths):
    skipped=[]
    chromesome=['','']
    out=lengths[0]
    inG=lengths[1]
    for row in range(0,len(df)):
        #put current chromosome in memory
        if chromesome[0]!=df.iloc[row,0]:
            chromesome[0]=df.iloc[row,0]
            chromesome[1]=genome[df.iloc[row,0]]
        #extract gene of interest
        Gene = df.iloc[row,:].values.tolist()
        if Gene[4]==1:
            #print("Pos strand", Gene)
            if Gene[3] < inG:
                skipped.append(Gene[1])
                continue
            else:
                strt=Gene[3]-inG
                end=Gene[3]+out
                if end > len(chromesome[1]):
                    skipped.append(Gene[1])
                    continue # skip genes that are too near the end of the chromosome, most of these are on scaffolds.
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end]
        else:
            #print("Neg strand", Gene)
            if Gene[2] < out:
                skipped.append(Gene[1])
                continue # skip genes that are too near the start of the chromosome, most of these are on scaffolds.
            else:
                strt=Gene[2]-out
                end=Gene[2]+inG
                if end > len(chromesome[1]):
                    skipped.append(Gene[1])
                    continue # skip genes that are too near the end of the chromosome, most of these are on scaffolds.
                gene_dict[Gene[1]]=chromesome[1].seq[strt:end].reverse_complement()   
    return gene_dict, skipped


# In[11]:

'''
#For checking extract_region_by_pos_term
gene_dict={}
in_file = "../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.gff3"
genome = '../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
gene_pos_df=get_gene_pos(in_file)
genome = SeqIO.index(genome, "fasta")
gene_dict, skipped=extract_region_by_pos_term(genome,gene_pos_df, gene_dict=gene_dict, lengths=[1000,500])
print len(skipped)
gene_pos_df[gene_pos_df["Gene_ID"].isin(skipped)]
#sanity check forward gene
GRMZM2G333069="CCACTCATCTATTTCTTTTGAATCATTGTATACTAGCTAGCTAGCTAGCCAGAGCTTGTATGGCCACTGCTCATCATGCGGATGACCATCGTGCAGGGAGAAGAAGCGAGGCCGTCGGAGAGAACTACATGAGGGGCCTCTACGGCGACGATGACTACAACGCCACCCACTACGGTCACCAGCAGCAGCAGCGGCCGCCGCCCGCGCCCATGGCGGTGGCCAAGGCGTTAGCCACAGCCACGGCAGCCTTCTCCATGTTGTTGTTGTCGGGGCTGGCGGTGACGGGCACGGTGCTGGCGCTCATCGTGGCGACGCCATTGATGGTGATCTTCAGCCCCGTGCTGGTGCCGGCGGCCATCACGGTGGCGCTGCTCACGGTAGGCATCGTTTCGTCGGGAGGGTTCGGCGTGGCTGCGGTGGCTGTGCTGGCGTGGGTGTACCGCTACCTTCAGACCACGACCTCGTCGTCGGGTCAGCAGCCGCACATCGTCAAGGACTGGGCGCAGCAGCACCGTCTGGAGCAGACACGCGCGCACTGAAATAAAAAAAAGCGTAGTACCCCTCCGTTTCTTTTTATTTGTTGCTGTTTAGTTCATATTTGTCACTGTTTCGTTAAAAATGAACTAGCGGGATAACGAGAACGGGAGGTAATATAGCTTTTCATTTAATGTAATGTAACGTAATCTTATGTTTGCAGCAGTAATAATGTGTATTATATATTTGCAGCAATAAAAGGGCGAGCTCGATCGACCCCTCTACGAATAAATACTTTAAAAAA"
print gene_dict["GRMZM2G333069"][:500]==GRMZM2G333069[-500:]
#reverse gene
GRMZM2G133819="TCAAACTCTGATTTTGTTGTGCAGCAATGCAGATAGAGTGACTAATAAGAACCAGTCCCAGGGTATAAGATTTCTTTCCCATGTTACAGGTAACCTATTTTCCCCCTTTAATAACTATAATCCTCATTCTAAACAAAGTAGCATCAAATTCGCAAACAACATAACTGACTATTGGTCGATAAACTCATGGCTACATGATTCAGCTAATATGGACAATATGGTACCAAAAGCCCCAACCACCGGCTCTAGGAATCAACAGAGATCGAAGGCCGGCTTAAACAAAAGGTCAGCCCAATATGTTGTGTGCTTATAATGGTCTTATGTTTTGATGATCAACAGCATCAAAATGAGCAAGCTTCTGGTTTGTAGTTTGTCATCATCTGGCGCCAAAGCCTTGCCCAAGGAGGGATTGCGAAAACCACAACAGGATGTTAAAAGGCAAAAGATGAACAATCCTACAGAGAATATTGTTGAGCTTGATGCGGTCAGCTCAGACGATGACGAGATAAATATAGTACTGCGACGCTGATGCTTCATCAATAATAGCGTGACATGTAATTGTACTCCAAATCGGCTAAACAATTGTACGCTCTTGTTTTCATATATCCCAAGTCATCCGAGGCAGAAAAATTAAATCAGATATATTTGTTGACTTATGGATCAGGTTGGTCCGTAGTGTTCTATTTTCTCTTGACATGCAGTTTAACAATTAGATATAAACATATATTTTTAATATTTTTATAGAAGAGAGAAGTCAGCCCTTGATCAAAAGTTA"
print gene_dict["GRMZM2G133819"][:500]==GRMZM2G133819[-500:]
'''


# In[12]:

def extract_region_by_pos_pro_term(genome, df, gene_dict, lengths):
    gene_dict_pro, skipped_pro=extract_region_by_pos(genome=genome,df=df, gene_dict={}, lengths=lengths)
    gene_dict_term, skipped_term=extract_region_by_pos_term(genome=genome,df=df, gene_dict={}, lengths=lengths)
    for gene in gene_dict_pro.keys():
        if gene in gene_dict_term.keys():
            gene_dict[gene]=Seq(gene_dict_pro[gene].tostring()+gene_dict_term[gene].tostring())
    skipped=set(skipped_pro+skipped_term)
    return gene_dict, skipped


# In[13]:

'''
#For checking extract_region_by_pos_pro_term
gene_dict={}
in_file = "../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.gff3"
genome = '../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
gene_pos_df=get_gene_pos(in_file)
genome = SeqIO.index(genome, "fasta")
gene_dict, skipped=extract_region_by_pos_pro_term(genome,gene_pos_df, gene_dict=gene_dict, lengths=[1000,500])
print len(skipped)
gene_pos_df[gene_pos_df["Gene_ID"].isin(skipped)]
#sanity check forward gene
GRMZM2G333069="CCACTCATCTATTTCTTTTGAATCATTGTATACTAGCTAGCTAGCTAGCCAGAGCTTGTATGGCCACTGCTCATCATGCGGATGACCATCGTGCAGGGAGAAGAAGCGAGGCCGTCGGAGAGAACTACATGAGGGGCCTCTACGGCGACGATGACTACAACGCCACCCACTACGGTCACCAGCAGCAGCAGCGGCCGCCGCCCGCGCCCATGGCGGTGGCCAAGGCGTTAGCCACAGCCACGGCAGCCTTCTCCATGTTGTTGTTGTCGGGGCTGGCGGTGACGGGCACGGTGCTGGCGCTCATCGTGGCGACGCCATTGATGGTGATCTTCAGCCCCGTGCTGGTGCCGGCGGCCATCACGGTGGCGCTGCTCACGGTAGGCATCGTTTCGTCGGGAGGGTTCGGCGTGGCTGCGGTGGCTGTGCTGGCGTGGGTGTACCGCTACCTTCAGACCACGACCTCGTCGTCGGGTCAGCAGCCGCACATCGTCAAGGACTGGGCGCAGCAGCACCGTCTGGAGCAGACACGCGCGCACTGAAATAAAAAAAAGCGTAGTACCCCTCCGTTTCTTTTTATTTGTTGCTGTTTAGTTCATATTTGTCACTGTTTCGTTAAAAATGAACTAGCGGGATAACGAGAACGGGAGGTAATATAGCTTTTCATTTAATGTAATGTAACGTAATCTTATGTTTGCAGCAGTAATAATGTGTATTATATATTTGCAGCAATAAAAGGGCGAGCTCGATCGACCCCTCTACGAATAAATACTTTAAAAAA"
print gene_dict["GRMZM2G333069"][1000:2000]==GRMZM2G333069[:500]+GRMZM2G333069[-500:]
#reverse gene
GRMZM2G133819="AAAAAAAAAAAAACCTCTCCCTCCAAAACTCCTCTCTTCCCCTTCCGAATTACAGTTTCCCTCGCATCCTCCTCCTACATCTATCTCCCACCTCGCACAATCACAGATTCCTGATTCCTCAATCCTCAGTACCCCGCTGCCTGTCCTCCACGCACCTCACCGCAAACATGGCGAACGCCGCCGACGACCTGGACCTCCTGCTATCACTAGGCGAGGCCGTCCCGGAGACACCTCCCGCCTCCCCCCGCACCGCCGATGTCCCCGTGTGCGACGGCGCTTTCACACCACCAAGGACGGCGCCCCCCGGCGGCACCGACATGTCCGTTTTCCGCGACGCCGTCAAGGATTACCTCGAGGCGGCCCCCGAAACCACCTCCCCGCTTGCTAATCGCCCCAAGCGACCCAAGGCCACCGAAATCCTCGTCGACAAATACTCCGGCCTCCGCATCAAGCACCTTACGCTCTCGCCTCTTGAGATTAGCAACCGCTTCGCTGACATCCGATTTGTGCGCATCACCGCGTTAAAGTGAGGATCCTTTTCCAGTTCGCCCCGCTGCTGGGTCATTCATGGTTTTTGCTCATCGATTCATCGGGTGGCTTTTCTTCCCTTTTCTGTTTCGCGTGCAGGAACTCTGTGGGGAGCGACAGGTTCTCCGGCTGTTGGGCAACCGCGGGCGTGTTACTGGACAAGGGCGTGCAGCGGGTGAGTGCGAAGGGGAGCAGCTACAGCATCTGGAAGATGGGCGCTCTGGACGAGACCGACGTATCTCTGTTCCTGTTTGGGGACGCGCACGTTCACTACTCCGGAGCTGCTGTGGGTTCAGTGTTCGCTGTGTTCAATGGCAATGTCCGCATGGACAATGGGGTACTTCTTATCTGACATTTTGATTTTCTTAATCACTTGGGTGATTTGTGCTCAGTGATTCCGTTGCTGATTCGAATGCGTGCTGGTGACGTTGTGTAGGGCAAAGGGTTCTCTATGAGTGTTGCTTCTGTTGGACAGATGTTGAAGATGGGGGTCGCATCAGATTTTGGTCTCTGCAAAGGCAAGAGGAAAGACGGGGTGGCTTGCACCATGGCTATAAATAAGTATGTACCTCTCAAACTCTGATTTTGTTGTGCAGCAATGCAGATAGAGTGACTAATAAGAACCAGTCCCAGGGTATAAGATTTCTTTCCCATGTTACAGGTAACCTATTTTCCCCCTTTAATAACTATAATCCTCATTCTAAACAAAGTAGCATCAAATTCGCAAACAACATAACTGACTATTGGTCGATAAACTCATGGCTACATGATTCAGCTAATATGGACAATATGGTACCAAAAGCCCCAACCACCGGCTCTAGGAATCAACAGAGATCGAAGGCCGGCTTAAACAAAAGGTCAGCCCAATATGTTGTGTGCTTATAATGGTCTTATGTTTTGATGATCAACAGCATCAAAATGAGCAAGCTTCTGGTTTGTAGTTTGTCATCATCTGGCGCCAAAGCCTTGCCCAAGGAGGGATTGCGAAAACCACAACAGGATGTTAAAAGGCAAAAGATGAACAATCCTACAGAGAATATTGTTGAGCTTGATGCGGTCAGCTCAGACGATGACGAGATAAATATAGTACTGCGACGCTGATGCTTCATCAATAATAGCGTGACATGTAATTGTACTCCAAATCGGCTAAACAATTGTACGCTCTTGTTTTCATATATCCCAAGTCATCCGAGGCAGAAAAATTAAATCAGATATATTTGTTGACTTATGGATCAGGTTGGTCCGTAGTGTTCTATTTTCTCTTGACATGCAGTTTAACAATTAGATATAAACATATATTTTTAATATTTTTATAGAAGAGAGAAGTCAGCCCTTGATCAAAAGTTA"
print gene_dict["GRMZM2G133819"][1000:2000]==GRMZM2G133819[:500]+GRMZM2G133819[-500:]
'''


# In[14]:

def gff_genome_to_oh_dict(gene_dict):
    num=0
    gene_oh={}
    for rec in gene_dict:
        gene_oh[rec] = seq_to_oh(str(gene_dict[rec])) 
        num+=1
        progress=num
        sys.stdout.write("Records converted to onehot: %d   \r" % (progress) )
        sys.stdout.flush()
    return gene_oh


# In[15]:

def extract_seqs_from_ref(genome_paths, gff3_paths):
    gene_dict={}
    for n in range(0,len(genome_paths)):
        in_file = gff3_paths[n] #'../../../../data/Zea_mays.AGPv3.31.gff3'
        genome = genome_paths[n] #'../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
        gene_pos_df=get_gene_pos(in_file)
        genome = SeqIO.index(genome, "fasta")
        gene_dict, added_Ns=extract_region_by_pos(genome,gene_pos_df, gene_dict=gene_dict)
    #gff_genome_to_hdf5(gene_dict)
    gene_oh = gff_genome_to_oh_dict(gene_dict)
    return gene_oh


# In[16]:

def extract_seqs_from_ref_phytozome(genome_paths, gff3_paths):
    gene_dict={}
    for n in range(0,len(genome_paths)):
        in_file = gff3_paths[n] #'../../../../data/Zea_mays.AGPv3.31.gff3'
        genome = genome_paths[n] #'../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
        if in_file=="../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.gff3":
            gene_pos_df=get_gene_pos(in_file)
        else:
            gene_pos_df=get_gene_pos_phytozome(in_file)
        genome = SeqIO.index(genome, "fasta")
        gene_dict, added_Ns=extract_region_by_pos(genome,gene_pos_df, gene_dict=gene_dict,lengths=[1000,500])
    #gff_genome_to_hdf5(gene_dict)
    gene_oh = gff_genome_to_oh_dict(gene_dict)
    return gene_oh


# In[17]:

def extract_seqs_from_ref_by_codon_pos(genome_paths, gff3_paths):
    gene_dict={}
    for n in range(0,len(genome_paths)):
        in_file = gff3_paths[n] #'../../../../data/Zea_mays.AGPv3.31.gff3'
        genome = genome_paths[n] #'../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
        gene_pos_df=get_strt_codon_pos_phytozome(in_file)
        genome = SeqIO.index(genome, "fasta")
        gene_dict, added_Ns, strt_codon_error=extract_region_by_strt_codon(genome,gene_pos_df, gene_dict=gene_dict)
    #gff_genome_to_hdf5(gene_dict)
    gene_oh = gff_genome_to_oh_dict(gene_dict)
    return gene_oh


# In[18]:

def extract_seqs_from_ref_pro_term_both(genome_paths, gff3_paths,regions, save_fastq, lengths, wrkdir):
    gene_dict={}
    for n in range(0,len(genome_paths)):
        in_file = gff3_paths[n]
        genome = genome_paths[n]
        if "Zea_mays" in in_file:
            gene_pos_df=get_gene_pos(in_file)
        else:
            gene_pos_df=get_gene_pos_phytozome(in_file)
        genome = SeqIO.index(genome, "fasta")
        if regions=="promoter":
            #print "promotor"
            gene_dict, skipped=extract_region_by_pos(genome,gene_pos_df, gene_dict=gene_dict, lengths=lengths)
        elif regions=="terminator":
            #print "terminator"
            gene_dict, skipped=extract_region_by_pos_term(genome,gene_pos_df, gene_dict=gene_dict, lengths=lengths)
        elif regions=="both":
            #print "both"
            gene_dict, skipped=extract_region_by_pos_pro_term(genome,gene_pos_df, gene_dict=gene_dict, lengths=lengths)
        else:
            print "regions available: promoter, terminator, or both"
    #Sanity check
    for key in gene_dict:
        if regions=="both":
            if len(gene_dict[key]) != (lengths[0]+lengths[1])*2:
                print "WARNING: SOME OUTPUT SEQUENCES ARE NOT THE CORRECT LENGTH. YOU JUST FOUND A NEW EDGE CASE IN THE CODE."
                print key
        else:
            if len(gene_dict[key]) != lengths[0]+lengths[1]:
                print "WARNING: SOME OUTPUT SEQUENCES ARE NOT THE CORRECT LENGTH. YOU JUST FOUND A NEW EDGE CASE IN THE CODE."
                print key
    gene_dict1={}
    for key in gene_dict:
        gene_dict1[key.split(".v")[0]]=gene_dict[key]
    if save_fastq:
        output = open(wrkdir+"/sb_zm_si_"+regions+".fasta","w")
        for gene in gene_dict1:
            output.write(">"+gene+"\n")
            output.write(str(gene_dict1[gene])+"\n")
    gene_oh = gff_genome_to_oh_dict(gene_dict1)
    return gene_oh


# In[19]:

def get_di_nuc_shuffled_gene_oh(regions):
    #import and onehot encode dinucliotide shuffled sequences
    if (regions=="promoter") or (regions=="both"):
        pro_dict = SeqIO.to_dict(SeqIO.parse("../../../../data/sb_zm_si_promoter_shuffled.fasta", "fasta"))
        pro_dict1={}
        for key in pro_dict:
            pro_dict1[key]=pro_dict[key].seq
    if regions=="terminator" or regions=="both":
        term_dict = SeqIO.to_dict(SeqIO.parse("../../../../data/sb_zm_si_terminator_shuffled.fasta", "fasta"))
        term_dict1={}
        for key in term_dict:
            term_dict1[key]=term_dict[key].seq
    if regions=="both":
        both_dict={}
        for key in pro_dict1:
            if key in term_dict1:
                both_dict[key]=Seq(pro_dict1[key].tostring()+term_dict1[key].tostring())
        gene_oh = gff_genome_to_oh_dict(both_dict)
    if regions=="promoter":
        gene_oh = gff_genome_to_oh_dict(pro_dict1)
    if regions=="terminator":
        gene_oh = gff_genome_to_oh_dict(term_dict1)
    if regions not in ["promoter", "terminator", "both"]:
        print "regions available: promoter, terminator, or both"
    return gene_oh


# In[20]:

'''
directory="../../../../data/Wide_taxon_data/"
gff3s=["Sbicolor_454_v3.1.1.gene_exons.gff3","Zea_mays.AGPv3.31.gff3","Sitalica_312_v2.2.gene_exons.gff3"]
genomes=["Sbicolor_454_v3.0.1.fa","Zea_mays.AGPv3.31.dna.toplevel.fa","Sitalica_312_v2.fa"]
genome_paths=[]
gff3_paths=[]
for num in range(0,len(gff3s)):
    gff3_paths.append(directory+gff3s[num])
    genome_paths.append(directory+genomes[num])
#print gff3_paths
#print genome_paths
gene_oh = extract_seqs_from_ref_pro_term_both(genome_paths,gff3_paths,regions="terminator", save_fastq=False, lengths=[2500,500])
'''


# In[28]:

#pickle.dump(gene_oh, open( "../../../../data/gene_oh1_term3kb.p", "wb" ))


# In[22]:

#for di-nucliotide shuffle: 
# 1) run extract_seqs_from_ref_pro_term_both(...save_fastq=True...)
# 2) if creating shuffled promoter and terminator together than run the above command twice (once with promoter and once with termintor)
# 3) run python ../../EXTREME-2.0.0/src/fasta-dinucleotide-shuffle.py -f sb_zm_si_promoter.fasta > sb_zm_si_promoter_shuffled.fasta (or similar)
# 4) run gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='promoter')  (or similar)

#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='both')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_pro_term1.5kb_shuffled.p", "wb" ))
#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='promoter')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_pro1.5kb_shuffled.p", "wb" ))
#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='terminator')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_term1.5kb_shuffled.p", "wb" ))
#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='both')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_pro_term3kb_shuffled.p", "wb" ))
#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='promoter')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_pro3kb_shuffled.p", "wb" ))
#gene_oh_di_nuc_shuff = get_di_nuc_shuffled_gene_oh(regions='terminator')
#pickle.dump(gene_oh_di_nuc_shuff, open( "../../../../data/gene_oh1_term3kb_shuffled.p", "wb" ))


# In[23]:

'''
directory="../../../../data/Wide_taxon_data/"
gff3s=["Sbicolor_454_v3.1.1.gene_exons.gff3","Zea_mays.AGPv3.31.gff3","Mo17_1.gff3"]
genomes=["Sbicolor_454_v3.0.1.fa","Zea_mays.AGPv3.31.dna.toplevel.fa","Mo17.fasta"]
genome_paths=[]
gff3_paths=[]
for num in range(0,len(gff3s)):
    gff3_paths.append(directory+gff3s[num])
    genome_paths.append(directory+genomes[num])
#print gff3_paths
#print genome_paths
gene_oh = extract_seqs_from_ref_by_codon_pos(genome_paths,gff3_paths)
'''


# In[24]:

#df = get_strt_codon_pos_phytozome("../../../../data/Wide_taxon_data/Sbicolor_454_v3.1.1.gene_exons.gff3")
#df = get_strt_codon_pos_phytozome("../../../../data/Wide_taxon_data/Mo17_1.gff3")
#df = get_strt_codon_pos_phytozome("../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.gff3")
#genome = SeqIO.index("../../../../data/Wide_taxon_data/Sbicolor_454_v3.0.1.fa", "fasta")
#genome = SeqIO.index("../../../../data/Wide_taxon_data/Mo17.fasta", "fasta")
#genome = SeqIO.index("../../../../data/Wide_taxon_data/Zea_mays.AGPv3.31.dna.toplevel.fa", "fasta")
#gene_dict, added_Ns, strt_codon_error=extract_region_by_strt_codon(genome,df,{})


# In[25]:

'''
directory="../../../../data/Wide_taxon_data/"
gff3s=["Phallii_308_v2.0.gene_exons.gff3","Sbicolor_454_v3.1.1.gene_exons.gff3",
       "Sitalica_312_v2.2.gene_exons.gff3","Zea_mays.AGPv3.31.gff3"]
genomes=["Phallii_308_v2.0.fa","Sbicolor_454_v3.0.1.fa","Sitalica_312_v2.fa","Zea_mays.AGPv3.31.dna.toplevel.fa"]
genome_paths=[]
gff3_paths=[]
for num in range(0,len(gff3s)):
    gff3_paths.append(directory+gff3s[num])
    genome_paths.append(directory+genomes[num])
#print gff3_paths
#print genome_paths
gene_oh = extract_seqs_from_ref_phytozome(genome_paths,gff3_paths)
'''


# In[26]:

'''
genome_paths=['../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa',
              '../../../../data/Sorghum_bicolor.Sorghum_bicolor_v2.dna.toplevel.fa']
gff3_paths=['../../../../data/Zea_mays.AGPv3.31.gff3',
           '../../../../data/Sorghum_bicolor.Sorghum_bicolor_v2.37.gff3']
#gene_oh = extract_seqs_from_ref(genome_paths,gff3_paths)
'''


# In[27]:

'''
#pickle
gene_dict={}
for n in range(0,len(genome_paths)):
    in_file = gff3_paths[n] #'../../../../data/Zea_mays.AGPv3.31.gff3'
    genome = genome_paths[n] #'../../../../data/Zea_mays.AGPv3.31.dna.toplevel.fa'
    gene_pos_df=get_gene_pos(in_file)
    genome = SeqIO.index(genome, "fasta")
    gene_dict, added_Ns=extract_region_by_pos(genome,gene_pos_df, gene_dict=gene_dict)
#import pickle
#pickle.dump(gene_dict, open( "Sequences_dict.p", "wb" ) )
#test = pickle.load( open( "Sequences_dict.p", "rb" ) )
#pd.DataFrame.from_dict(gene_dict, orient='index').to_csv('Homolog_sequence.csv')
'''


# In[ ]:



