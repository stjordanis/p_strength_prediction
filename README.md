#Evolutionarily informed deep learning methods: Predicting transcript abundance from DNA sequence#

If you use this repository or the ideas contained in it please kindly cite:

--final manuscript citation here--

### How do I get set up? ###

1) Install the following:

* Python 2
* Tensorflow
* Keras
* Jupyter Notebook

2) Clone this repository

3) Download the test data from: https://de.cyverse.org/dl/d/0F63CD73-C1BE-4588-8FEF-E9EF57FCA117/data.tar.gz

4) Untar the downloaded file (its contents should replace those of the current data directory)

### How do I run the scripts? ###

####Pseudo-gene Model####
All scripts for the pseudogene model can be found in the src/main/python and src/main/R directories.

Genes were divided into families by using the Generate_paralogous_gene_pairs.R script from src/main/R and construct_gene_families.py script from src/main/python.
Promoter and Terminator sequences were extracted from the maize genome using the Generate_sequences_as_predictors.R script in src/main/R.

To train pseudogene model using family-guided 10 times 5-fold cross-validation with the "off/on" gene set, run the train_models_un_vs_expr_masked.py script from src/main/python.
To train pseudogene model using family-guided 10 times 5-fold cross-validation with the "off/high" gene set, run the train_models_un_vs_high_masked.py script from src/main/python.
To calculate the saliency map for each gene, run the saliency_un_vs_expr.py or saliency_un_vs_high.py script from src/main/python.
To calculate the average saliency map, run summarise_saliency.py from src/main/python. Results can be found in the 'data' directory.

####Contrast Model####

All scripts for the contrast model can be found in the notebook directory.
To run k-folds cross-validation as done in the manuscript simply open and run the "K-folds_Ortholog_contrasts_sb_zm_single_copy.ipynb" script in Jupyter Notebook. Figures and statistics from the model run will be output to the figs directory.
